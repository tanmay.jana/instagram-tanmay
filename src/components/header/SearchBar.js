import React from "react";
import "./SearchBar.css";
import logo2 from "../images/insta.png";

export default function SearchBar() {
  return (
    <div className="header">
      <div className="logo-container">
        <img
          className="logo1"
          src={"https://img.icons8.com/metro/26/000000/instagram-new.png"}
          alt="o"
        />
        <img className="logo2" src={logo2} alt="insta logo" />
      </div>
      <div className="search-container">
        <input
          className="search"
          type="text"
          name="Search"
          placeholder="&#9906; Search"
        />
      </div>
      <div className="nav-container">
        <img
          src="https://img.icons8.com/material-outlined/24/000000/compass.png"
          alt="logo"
        />
        <img src="https://img.icons8.com/metro/26/000000/like.png" alt="logo" />
        <img
          src="https://img.icons8.com/windows/32/000000/contacts.png"
          alt="logo"
        />
      </div>
    </div>
  );
}
