import React from 'react'
import './Post.css';

export default function Feed(props) {
    console.log(props)
  return (
    <div>
      <img src={props.value.imageUrl} alt='img' className="feed-img"/>
      <div className='logo-content'>
      <i className="fa fa-heart-o"></i>
      <i class="fa fa-comment-o"></i>
      </div>
      <p className='likes'>{props.value.likes} Likes</p>
    </div>
  )
}
