import React from "react";
import Thumbnail from "./Thumbnail";
import "./Post.css";

export default function Post(props) {
  // console.log(props.value.data)
  return (
    <div className='post-content'>
      {props.value.data.map(x => {
        return <Thumbnail value={x} />;
      })}
    </div>
  );
}
