import React from "react";
import "./Post.css";
import Feed from "./Feed";

export default function Thumbnail(props) {
//   console.log(props.value.thumbnailUrl);
  return (
    <div>
      <div className="thumbnail-container">
        <img className="thumb-img" src={props.value.thumbnailUrl} alt="logo" />
        <p className="name">{props.value.username}</p>
      </div>
      <Feed value={props.value}/>
    </div>
  );
}
