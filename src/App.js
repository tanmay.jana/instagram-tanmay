import React, { Component } from 'react';
import Search from './components/header/SearchBar';
import Post from './components/postContainer/Post'
import './App.css';
import data from './dummy-data'

class App extends Component {
  state = {data: {data}};
  render() {
    return (
      <div className="App">
      <Search/>
      <Post value = {this.state.data}/>
      </div>
    );
  }
}

export default App;
